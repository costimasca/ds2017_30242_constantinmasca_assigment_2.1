package ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

/**
 * Created by constantin on 11/6/17.
 */
public interface ISellPriceService {

    /**
     * Computes the tax to be payed for a Car.
     *
     * @param c Car for which to compute the sell price
     * @return selling price for the car
     */
    double computeSellPrice(Car c);
}
