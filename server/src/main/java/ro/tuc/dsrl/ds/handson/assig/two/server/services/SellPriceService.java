package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ISellPriceService;

/**
 * Created by constantin on 11/6/17.
 */
public class SellPriceService implements ISellPriceService {

    @Override
    public double computeSellPrice(Car c) {

        if (c.getPrice() <= 0) {
            throw new IllegalArgumentException("Purchase price must be positive.");
        }

        double price = c.getPrice() - c.getPrice() / 7 *(2015 - c.getYear());

        return price;
    }
}
